# Zeta
## Name
ZetaTravel Companion
## Description
During the pandemic there was uncertainty about the future. Preparing a journey was not easy. We want to give more flexibility to the travellers.
The idea is to allow people to buy planes tickets (just these tickets, for the moment) giving the possibility to change their plans.
In order to fulfill this objective, we came up with the idea of a marketplace.
Moreover, we aim to provide the user with all information about the place they want to go. The application should monitor COIVD-19 cases globally to elaborates previsions to help users to forecast events such as lockdowns.
A machine learning algorithm should help users to find out the best time to buy tickets.

## Installation
At this stage the application is runnable through local host but it is possible to load it on a server.

## Roadmap
- Finish the interface
- Develop learners

## Authors and acknowledgment
Gianluca Ruberto and Giacomo Pizzamiglio

## Project status
The project is, obviously, not finished. At the moment is possible to put the ticket that you want to sell in a database. The same database is used to search a second hand ticket when you want to buy one. But we wanted to create a portal that can help all the traveller. We added a section that is used to search for a "first hand" flight that looks for the best price. The home page is at the moment empty, but in our idea there would be all the information that could help people to travel.
The machine learning side of the project is not fully developed: at this stage only covid data apis and tickets apis are implemented. The plan is to add machine learning algorithms and neural networks to elaborate prevision on COVID-19 pandemic evolution and tickets price changes.