import json

from Project.scripts.api_client.rest_api_client import RestAPIClient


class SkyscannerAPIClient(RestAPIClient):
    __headers = {
        'x-rapidapi-host': "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
        'x-rapidapi-key': "c52a5eeeb3msh455c61b04253946p11406bjsn67d012c60105"
    }

    def list_places(self, place: str, country: str = "IT", currency: str = "EUR", locale: str = "en-GB"):
        url = f"https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/autosuggest/v1.0/{country}/{currency}/{locale}/"
        params = {"query": place}
        return self._handle_request(url, headers=self.__headers, params=params)

    def list_currencies(self):
        url = "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/reference/v1.0/currencies"
        return self._handle_request(url, headers=self.__headers)

    def list_markets(self):
        url = "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/reference/v1.0/countries/en-US"
        return self._handle_request(url, headers=self.__headers)

    def search_flight(self, country, currency, origin, destination, outbound_date, inbound_date="anytime", locale="en-GB"):
        url = f"https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browseroutes/v1.0/{country}/{currency}/{locale}/{origin}/{destination}/{outbound_date}"
        params = {"inboundpartialdate": inbound_date}
        return self._handle_request(url, headers=self.__headers, params=params)


if __name__ == '__main__':
    api = SkyscannerAPIClient()
    print(api.list_markets())
