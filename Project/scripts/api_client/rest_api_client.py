import json
import requests


class RequestsCache:

    def __init__(self, max_length=50):
        self.__max_length = 50
        self.__cache = []

    def add(self, key: str, value: str, headers=None, params=None):
        if len(self.__cache) == self.__max_length:
            self.__cache.pop(0)
        self.__cache.append({
            "key": key,
            "headers": headers,
            "params": params,
            "value": value
        })

    def clear(self):
        self.__cache = []

    def get(self, key: str):
        for elem in self.__cache:
            if elem["key"] == key:
                return elem["value"]
        return None


class RestAPIClient:

    def __init__(self):
        self.__session = requests.Session()
        self.__cache = RequestsCache()

    def _handle_request(self, url, headers=None, params=None):
        response = self.__session.request("GET", url, headers=headers, params=params)
        self.__cache.add(url, json.dumps(response.json()))
        return response.json()
