import json

from Project.scripts.api_client.rest_api_client import RestAPIClient


class CovidAPIClient(RestAPIClient):
    api_endpoint = "https://corona.lmao.ninja/v2"
    continents_url = "/continents"
    countries_url = "/countries"
    historical_url = "/historical"

    def get_all_continents(self, yesterday: bool = False, sort: str = None):
        sort = "" if None else f"={sort}"
        request = f"{self.api_endpoint}{self.continents_url}?yesterday={yesterday}&sort{sort}"
        return self._handle_request(request)

    def get_single_continent(self, continent: str, yesterday: bool = False, strict: bool = False):
        request = f"{self.api_endpoint}{self.continents_url}/{continent}?yesterday={yesterday}&strict{strict}"
        return self._handle_request(request)

    def get_all_countries(self, yesterday: bool = False, sort: str = None):
        sort = "" if None else f"={sort}"
        request = f"{self.api_endpoint}{self.countries_url}?yesterday?{yesterday}&sort{sort}"

    def get_single_country(self, country: str, yesterday: bool = False, strict: bool = False):
        request = f"{self.api_endpoint}{self.countries_url}/{country}?yesterday={yesterday}&strict{strict}&query"
        return self._handle_request(request)

    def get_historic_data(self, country: str, days: int = 30):
        request = f"{self.api_endpoint}{self.historical_url}/{country}?lastdays={days}"
        return self._handle_request(request)

    def get_province_historic_data(self, country: str, province: str, days: int = 30):
        request = f"{self.api_endpoint}{self.historical_url}/{country}/{province}?lastdays={days}"
        return self._handle_request(request)
