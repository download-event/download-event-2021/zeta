from dash import html
import dash_bootstrap_components as dbc

from Project.components.pages.buy_ticket import buypage_layout
from Project.components.pages.sell_ticket import sellpage_layout
from Project.components.pages.search_flight import load_search_page


def render_page_content(pathname):
    if pathname == '/buy':
        return buypage_layout
    elif pathname == "/sell":
        return sellpage_layout
    elif pathname == "/search":
        return load_search_page()
    elif pathname == "/":
        return html.Div(
            [
            html.H1("Home"),
            html.Hr(),
            html.P("Find out all about the place you dream to go. Receive current data about COIVD-19 and previsions made by AI.")
            ]
        )
    else:
        return None
