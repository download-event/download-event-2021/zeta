import json

from dash import html

from Project.components.pages.buy_ticket import buypage_layout
from Project.components.pages.sell_ticket import sellpage_layout
from Project.components.pages.search_flight import load_search_page
from Project.scripts.api_client import SkyscannerAPIClient


def load_airports(place, country, currency):
    api = SkyscannerAPIClient()
    places = api.list_places(place=place, country=country, currency=currency)

    if "Places" in places.keys():
        places = places["Places"]
        return [{"label": elem["PlaceName"], "value": elem["PlaceId"]} for elem in places]
    else:
        return []


def load_flight(currency, origin, inbound_date, outbound_date, country, destination):
    api = SkyscannerAPIClient()
    routes = api.search_flight(country=country, currency=currency, origin=origin, destination=destination, inbound_date=inbound_date, outbound_date=outbound_date)
    if "Quotes" in routes.keys():
        return html.P(json.dumps(routes["Quotes"]))
    else:
        return None
