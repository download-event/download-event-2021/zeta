import dash_bootstrap_components as dbc
from dash import html

SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

SIDEBAR = html.Div(
    [
        html.H2(
            [
                html.Span("Zeta", className="text-primary"),
                "ZetaTravel"
            ],
            className="display-5"),
        html.Hr(),
        html.P(
            "Travel made easy", className="lead"
        ),
        dbc.Nav(
            [
                dbc.NavLink("Home", href="/", active="exact"),
                dbc.NavLink("Search Flight", href="/search", active="exact"),
                dbc.NavLink("Smart Ticket", href="/buy", active="exact"),
                dbc.NavLink("Cancel Travel", href="/sell", active="exact"),
                # dbc.NavLink("Emergency", href="/emergency", active="exact"),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)
