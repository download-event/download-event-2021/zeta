from dash import html
from dash import dcc
import sqlite3
import dash_bootstrap_components as dbc


from datetime import date

left_pixel_margin = "0"

sellpage_layout = html.Div([
    html.H1("Sell your ticket!",
            style={'marginLeft': left_pixel_margin}
            ),
    html.P("Select the departure airport",
           style={'marginLeft': left_pixel_margin}),
    dbc.Input(
        id={"type": "dynamic-input", "name": "departure_airport_input_sell".format("search")},
        type="search",
        placeholder="input type {}".format("search"),
        style={'marginLeft': left_pixel_margin}
    ),
    html.P("Select the arrival airport",
           style={'marginLeft': left_pixel_margin}),
    dbc.Input(
        id={"type": "dynamic-input", "name": "arrival_airport_input_sell".format("search")},
        type="search",
        placeholder="input type {}".format("search"),
        style={'marginLeft': left_pixel_margin}
    ),
    html.P("Select the date",
           style={'marginLeft': left_pixel_margin}),
    dcc.DatePickerSingle(
        id={"type": "dynamic-input", "name": "date_picker_input_sell".format("search")},
        min_date_allowed=date.today(),
        max_date_allowed=date(2023, 1, 1),
        initial_visible_month=date.today(),
        date=date.today(),
        style={'marginLeft': left_pixel_margin}
    ),
    html.P("Insert the ticket's price",
           style={'marginLeft': left_pixel_margin}),
    dbc.Input(
        id={"type": "dynamic-input", "name": "ticket's_price_input".format("number")},
        type="number",
        placeholder="0.00$",
        style={'marginLeft': left_pixel_margin}
    ),
    html.P("Select the change's person price",
           style={'marginLeft': left_pixel_margin}),
    dbc.Input(
        id={"type": "dynamic-input", "name": "person's_price_input".format("number")},
        type="number",
        placeholder="0.00$",
        style={'marginLeft': left_pixel_margin}
    ),
    html.Div(
        [
            dbc.Button('Sell', id={"type": "dynamic-input", "name": "sell_button"}, n_clicks=0, className="bg-danger"),
        ],
        className="d-flex flex-row justify-content-center align-items-center",
        style={"padding": "1rem"}
    ),
    dcc.Textarea(
        id={"type":"dynamic-input", "name": "text-placeholder"},
        value='Textarea content initialized\nwith multiple lines of text',
        style={'width': 1, 'height': 1, 'disabled': True, 'hidden': True},
    )
]
)


def insertInTable(n_clicks, departure, arrival, date,price,person):
    if n_clicks > 0:
         con = sqlite3.connect('example.db')
         cur = con.cursor()
         try:
            cur.execute("CREATE TABLE flights (departure text,"
                        "arrival text, date date, price real,"
                        "change real)")
         except:
             print("il database è già stato creato")
         finally:
           cur.execute(f"INSERT INTO flights VALUES ('{departure}', '{arrival}', '{date}', '{price}', '{person}')")
           con.commit()
           con.close()
           con = sqlite3.connect('example.db')
           cur = con.cursor()
           cur.execute("INSERT INTO flights VALUES ('milan', 'cagliari', '2021-10-10', 50, 12.3)")
           con.commit()
           for row in cur.execute("SELECT * from flights"):
                print(row)
           con.close()