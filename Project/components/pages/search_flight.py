import dash_bootstrap_components as dbc
from dash import html
from dash import dcc
from datetime import date

import sqlite3
import pandas as pd

from Project.components.pages.buy_ticket import buypage_layout
from Project.scripts.api_client import SkyscannerAPIClient


def load_search_page():
    api = SkyscannerAPIClient()
    countries = api.list_markets()['Countries']
    currencies = api.list_currencies()['Currencies']

    search_flight_layout = html.Div(
        [
            html.H1("Search Flights"),
            html.Hr(),
            html.Div(
                [
                    html.Div(
                        [
                            dcc.Dropdown(id={"type": "dynamic-dropdown", "name": "market-dropdown"},
                                         multi=False, clearable=True, placeholder="Select your country",
                                         options=[{"label": elem["Name"], "value": elem["Code"]} for elem in
                                                  countries if
                                                  elem["Code"] == "IT"],
                                         value="IT",
                                         style={"min-width": "15rem"}),
                            dcc.Dropdown(id={"type": "dynamic-dropdown", "name": "currency-dropdown"},
                                         multi=False, clearable=True, placeholder="Select your currency",
                                         options=[{"label": elem["Symbol"], "value": elem["Code"]} for elem in
                                                  currencies if
                                                  elem["Code"] == "EUR"],
                                         value="EUR",
                                         style={"min-width": "15rem"})
                        ],
                        className="d-flex flex-row align-items-center",
                        style={"width": "100%", "justify-content": "space-evenly", "margin": "2rem 0"}
                    ),
                    html.Div(
                        [
                            dbc.Input(
                                id={"type": "dynamic-input", "name": "origin"},
                                placeholder="Enter an origin place",
                                type="text",
                                style={"max-width": "15rem"}
                            ),
                            dbc.Input(
                                id={"type": "dynamic-input", "name": "destination"},
                                placeholder="Enter a destination place",
                                type="text",
                                style={"max-width": "15rem"}
                            )
                        ],
                        className="d-flex flex-row align-items-center",
                        style={"width": "100%", "justify-content": "space-evenly"}
                    ),
                    html.Div(
                        [
                            dcc.Dropdown(id={"type": "dynamic-dropdown", "name": "origin-airport"},
                                         multi=False, clearable=True, placeholder="Select an origin airport",
                                         options=[],
                                         style={"min-width": "15rem"}),
                            dcc.Dropdown(id={"type": "dynamic-dropdown", "name": "destination-airport"},
                                         multi=False, clearable=True, placeholder="Select a destination airport",
                                         options=[],
                                         style={"min-width": "15rem"})
                        ],
                        className="d-flex flex-row align-items-center",
                        style={"width": "100%", "justify-content": "space-evenly", "margin-top": "1rem"}
                    ),
                    html.Div(
                        [
                            dbc.Input(
                                id={"type": "dynamic-input", "name": "inbound-date"},
                                placeholder="Select the inbound date",
                                type="date",
                                style={"max-width": "15rem"}
                            ),
                            dbc.Input(
                                id={"type": "dynamic-input", "name": "outbound-date"},
                                placeholder="Select the outbound date",
                                type="date",
                                style={"max-width": "15rem"}
                            )
                        ],
                        className="d-flex flex-row align-items-center",
                        style={"width": "100%", "justify-content": "space-evenly", "margin-top": "1rem"}
                    ),
                    html.Div(
                        dbc.Button(
                            "SEARCH",
                            id={"type": "dynamic-button", "name": "search-flight"},
                            style={"width": "20%", "margin-top": "2rem"},
                            className="bg-success",
                            n_clicks=0
                        ),
                        className="d-flex flex-row justify-content-center align-items-center"
                    ),
                    html.Div(
                        id={"type": "dynamic-content", "name": "flight-info"}
                    )
                ],
                className="d-flex flex-column"
            )
        ]
    )

    return search_flight_layout


