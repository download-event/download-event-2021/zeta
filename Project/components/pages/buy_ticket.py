from dash import html
from dash import dcc
from dash import dash_table
from datetime import date

import sqlite3
import pandas as pd
import dash_bootstrap_components as dbc

con = sqlite3.connect('example.db')
cur = con.cursor()
df = pd.read_sql("SELECT * FROM flights", con)
con.close()

left_pixel_margin = "0"

buypage_layout = html.Div([
    html.H1("Buy a last minute second hand ticket and save money!",
            style={'marginLeft': left_pixel_margin}
            ),
    html.P("Select the departure airport",
           style={'marginLeft': left_pixel_margin}),
    dbc.Input(
        id={"type": "dynamic-input", "name": "departure_airport_input".format("search")},
        type="search",
        placeholder="input type {}".format("search"),
        style={'marginLeft': left_pixel_margin}
    ),
    html.P("Select the arrival airport",
           style={'marginLeft': left_pixel_margin}),
    dbc.Input(
        id={"type": "dynamic-input", "name": "arrival_airport_input".format("search")},
        type="search",
        placeholder="input type {}".format("search"),
        style={'marginLeft': left_pixel_margin}
    ),
    html.P("Select the date",
           style={'marginLeft': left_pixel_margin}),
    dcc.DatePickerSingle(
        id={"type": "dynamic-input", "name": "date_picker_input".format("search")},
        min_date_allowed=date.today(),
        max_date_allowed=date(2023, 1, 1),
        initial_visible_month=date.today(),
        date=date.today(),
        style={'marginLeft': left_pixel_margin}
    ),
    html.Div(
        [
            dbc.Button('Search', id={"type": "dynamic-input", "name": "search_button"}, n_clicks=0, className="bg-success"),
        ],
        className="d-flex flex-row justify-content-center align-items-center",
        style={"padding": "1rem"}
    ),
    html.Div(id={"type": "dynamic-input", "name": "buy_ticket"}),
    dash_table.DataTable(
        id={"type": "dynamic-input", "name": "buy_table"},
        columns=[{"name": i, "id": i} for i in df.columns],
        data=df.to_dict('records'),
        style_cell_conditional=[
            {'if': {'column_id': 'price'},
             'width': '5%'},
            {'if': {'column_id': 'change'},
             'width': '5%'
             },
            {'if': {'column_id': 'date'},
             'width': '15%'
             },
            {'if': {'column_id': 'arrival'},
             'width': '15%'
             },
            {'if': {'column_id': 'departure'},
             'width': '15%'
             }
        ]
       )
]
)


def showSelectedTickets(n_clicks, departure, arrival, date):
    if n_clicks > 0:
        con = sqlite3.connect('example.db')
        cur = con.cursor()
        df = pd.read_sql(f"SELECT * FROM flights WHERE departure == '{departure}' AND arrival == '{arrival}' AND date == '{date}'", con)
        #df = pd.read_sql( f"SELECT * FROM flights WHERE date == '2021-10-10'", con)
        con.close()
        return df.to_dict("records")
    else:
        con = sqlite3.connect('example.db')
        cur = con.cursor()
        df = pd.read_sql("SELECT * FROM flights", con)
        con.close()
        return df.to_dict("records")


