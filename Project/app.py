import dash
import dash_bootstrap_components as dbc
from dash import html
from dash import dcc
from dash.dependencies import Input, Output, State

from Project.components.callbacks.routing import render_page_content
from Project.components.callbacks.load_airports import load_airports, load_flight
from components.sidebar import SIDEBAR
from Project.components.pages.buy_ticket import showSelectedTickets
from Project.components.pages.sell_ticket import insertInTable

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

app.layout = html.Div([dcc.Location(id='url', refresh=False),
                       html.Div(id='page-content', children=[], style=CONTENT_STYLE),
                       SIDEBAR])


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def handle_render_page_content(pathname):
    return render_page_content(pathname)


@app.callback(
    Output({"type": "dynamic-input", "name": "buy_table"}, 'data'),
    Input({"type": "dynamic-input", "name": "search_button"}, 'n_clicks'),
    State({"type": "dynamic-input", "name": "departure_airport_input".format("search")}, 'value'),
    State({"type": "dynamic-input", "name": "arrival_airport_input".format("search")}, 'value'),
    State({"type": "dynamic-input", "name": "date_picker_input".format("search")}, 'date'))
def show_Selected_Tickets(n_clicks, departure, arrival, date):
    return showSelectedTickets(n_clicks,departure, arrival, date)



@app.callback(
    Output({"type": "dynamic-dropdown", "name": "origin-airport"}, "options"),
    Input({"type": "dynamic-input", "name": "origin"}, "value"),
    State({"type": "dynamic-dropdown", "name": "market-dropdown"}, "value"),
    State({"type": "dynamic-dropdown", "name": "currency-dropdown"}, "value"),
)
def handle_origin_airports(place, country, currency):
    return load_airports(place, country, currency)


@app.callback(
    Output({"type": "dynamic-dropdown", "name": "destination-airport"}, "options"),
    Input({"type": "dynamic-input", "name": "destination"}, "value"),
    State({"type": "dynamic-dropdown", "name": "market-dropdown"}, "value"),
    State({"type": "dynamic-dropdown", "name": "currency-dropdown"}, "value"),
)
def handle_destination_airports(place, country, currency):
    return load_airports(place, country, currency)

@app.callback(
    Output({"type": "dynamic-content", "name": "flight-info"}, "children"),
    Input({"type": "dynamic-button", "name": "search-flight"}, "n_clicks"),
    State({"type": "dynamic-dropdown", "name": "market-dropdown"}, "value"),
    State({"type": "dynamic-dropdown", "name": "currency-dropdown"}, "value"),
    State({"type": "dynamic-dropdown", "name": "origin-airport"}, "value"),
    State({"type": "dynamic-dropdown", "name": "destination-airport"}, "value"),
    State({"type": "dynamic-input", "name": "outbound-date"}, "value"),
    State({"type": "dynamic-input", "name": "inbound-date"}, "value"),
)
def handle_search_flight(n_clicks, country, currency, origin, destination, outbound_date, inbound_date):
    if n_clicks >= 1:
        return load_flight(currency, origin, inbound_date, outbound_date, country, destination)
    else:
        return None

@app.callback(
    Output({"type": "dynamic-input", "name": "text-placeholder"},'value'),
    Input({"type": "dynamic-input", "name": "sell_button"}, 'n_clicks'),
    State({"type": "dynamic-input", "name": "departure_airport_input_sell".format("search")}, 'value'),
    State({"type": "dynamic-input", "name": "arrival_airport_input_sell".format("search")}, 'value'),
    State({"type": "dynamic-input", "name": "date_picker_input_sell".format("search")}, 'date'),
    State({"type": "dynamic-input", "name": "ticket's_price_input".format("number")}, 'value'),
    State({"type": "dynamic-input", "name": "person's_price_input".format("number")},'value'))
def insert_In_Table(n_clicks, departure, arrival, date,price, person):
    insertInTable(n_clicks, departure, arrival, date, price, person)
    return "è stato premuto sell"


if __name__ == '__main__':
    app.run_server(debug=True)
